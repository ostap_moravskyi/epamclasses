using System;
using System.Text;

namespace Epam.Tasks.Matrix
{
	class Matrix
	{
		public Matrix(int rows, int columns)
		{
			_array = new double[columns, rows];
		}
		
		public int Length
		{
			get
			{
				return _array.Length;
			}
		}
		public double this[int i, int j]
		{
			get
			{
				if (CorrespondToArraySize(i, j))
				{
					return _array[i, j];
				}
				else
				{
					throw new IndexOutOfRangeException();
				}
			}
			set
			{
				if (CorrespondToArraySize(i, j))
				{
					_array[i, j] = value;
				}
				else
				{
					throw new IndexOutOfRangeException();
				}
			}
		}
		
		public override bool Equals(object obj)
		{
			Matrix other = obj as Matrix;
			
			if ((obj == null) || (GetLength(0) != other.GetLength(0)) || (GetLength(1) != other.GetLength(1)))
			{
				return false;
			}
			else
			{
				for (var i = 0; i < GetLength(0); i++)
				{
					for (var j = 0; j < GetLength(1); j++)
					{
						if (this[i, j] != other[i, j])
						{
							return false;
						}
					}
				}
				
				return true;
			}
		}
		public override int GetHashCode()
		{
			int hash = 7;
			
			for (var i = 0; i < GetLength(0); i++)
			{
				for (var j = 0; j < GetLength(1); j++)
				{
					hash = hash * 13 + this[i, j].GetHashCode();
				}
			}
			
			return hash;	
		}
		public override string ToString()
		{
			StringBuilder str = new StringBuilder("{", Length * 4);
			
			for (var i = 0; i < GetLength(0); i++)
			{
				str.Append("{");
				for (var j = 0; j < GetLength(1); j++)
				{
					str.Append(this[i, j]);
					str.Append(";");
				}
				str.Append("}");
			}
			str.Append("}");
			
			return str.ToString();
		}
		
		public int GetLength(int dimension)
		{
			if ((dimension == 0) || (dimension == 1))
			{
				return _array.GetLength(dimension);
			}
			else
			{
				throw new IndexOutOfRangeException();
			}
		}
		public void Add(Matrix other)
		{
			if ((GetLength(0) != other.GetLength(0)) || (GetLength(1) != other.GetLength(1)))
			{
				throw new IndexOutOfRangeException();
			}
			else
			{
				
				for (var i = 0; i < GetLength(0); i++)
				{
					for (var j = 0; j < GetLength(1); j++)
					{
						this[i, j] += other[i, j];
					}
				}
			}
		}
		public void Subtract(Matrix other)
		{
			if ((GetLength(0) != other.GetLength(0)) || (GetLength(1) != other.GetLength(1)))
			{
				throw new IndexOutOfRangeException();
			}
			else
			{
				for (var i = 0; i < GetLength(0); i++)
				{
					for (var j = 0; j < GetLength(1); j++)
					{
						this[i, j] -= other[i, j];
					}
				}
			}
		}
		public void Multiply(double mult)
		{
			for (var i = 0; i < GetLength(0); i++)
			{
				for (var j = 0; j < GetLength(1); j++)
				{
					this[i, j] *= mult;
				}
			}
		}
		public Matrix SubMatrix(int startColumn, int startRow, int width, int height)
		{
			Matrix subMatrix = new Matrix(width, height);
			
			for (var i = 0; i < subMatrix.GetLength(0); i++)
			{
				for (var j = 0; j < subMatrix.GetLength(1); j++)
				{
					subMatrix[i, j] = this[i + startColumn, j + startRow];
				}
			}
			
			return subMatrix;
		}
		
		public static Matrix Add(Matrix first, Matrix second)
		{
			if ((first.GetLength(0) != second.GetLength(0)) || (first.GetLength(1) != second.GetLength(1)))
			{
				throw new IndexOutOfRangeException();
			}
			else
			{
				Matrix result = new Matrix(first.GetLength(0), first.GetLength(1));
				
				for (var i = 0; i < result.GetLength(0); i++)
				{
					for (var j = 0; j < result.GetLength(1); j++)
					{
						result[i, j] = first[i, j] + second[i, j];
					}
				}
				
				return result;
			}
		}
		public static Matrix Subtract(Matrix first, Matrix second)
		{
			if ((first.GetLength(0) != second.GetLength(0)) || (first.GetLength(1) != second.GetLength(1)))
			{
				throw new IndexOutOfRangeException();
			}
			else
			{
				Matrix result = new Matrix(first.GetLength(0), first.GetLength(1));
				
				for (var i = 0; i < result.GetLength(0); i++)
				{
					for (var j = 0; j < result.GetLength(1); j++)
					{
						result[i, j] = first[i, j] - second[i, j];
					}
				}
				
				return result;
			}
		}
		public static Matrix Multiply(Matrix matrix, double mult)
		{
			Matrix result = new Matrix(matrix.GetLength(0), matrix.GetLength(1));
				
			for (var i = 0; i < result.GetLength(0); i++)
			{
				for (var j = 0; j < result.GetLength(1); j++)
				{
					result[i, j] = matrix[i, j] * mult;
				}
			}
			
			return result;
		}
		public static Matrix Multiply(Matrix first, Matrix second)
		{
			if (first.GetLength(0) != second.GetLength(1))
			{
				throw new IndexOutOfRangeException();
			}
			else
			{
				Matrix result = new Matrix(first.GetLength(1), second.GetLength(0));
				
				for (var i = 0; i < result.GetLength(0); i++)
				{
					for (var j = 0; j < result.GetLength(1); j++)
					{
						for (var k = 0; k < first.GetLength(0); k++)
						{
							result[i, j] += first[i, k] * second[k, j];
						}
					}
				}
				
				return result;
			}
		}
		public static Matrix Transpose(Matrix matrix)
		{
			Matrix result = new Matrix(matrix.GetLength(1), matrix.GetLength(0));
			
			for (var i = 0; i < result.GetLength(0); i++)
			{
				for (var j = 0; j < result.GetLength(1); j++)
				{ 
					result[i, j] = matrix[j, i];
				}
			}
			
			return result;
		}
		
		public static bool operator ==(Matrix first, Matrix second)
		{
			return first.Equals(second);
		}
		public static bool operator !=(Matrix first, Matrix second)
		{
			return !first.Equals(second);
		}
		public static Matrix operator +(Matrix first, Matrix second)
		{
			return Add(first, second);
		}
		public static Matrix operator -(Matrix first, Matrix second)
		{
			return Subtract(first, second);
		}
		public static Matrix operator *(Matrix matrix, double mult)
		{
			return Multiply(matrix, mult);
		}
		public static Matrix operator *(double mult, Matrix matrix)
		{
			return Multiply(matrix, mult);
		}
		public static Matrix operator *(Matrix first, Matrix second)
		{
			return Multiply(first, second);
		}
		
		private double[,] _array;
		
		private bool CorrespondToArraySize(int i, int j)
		{
			bool positive = (i >= 0) && (j >= 0);
			bool lessThanSize = (i < GetLength(0)) && (j < GetLength(1));
			
			return (positive && lessThanSize);
		}
	}
	
	class RepresentingMatrix
	{
		static void Main()
		{
			// Creating and initializing new matrices
			var matrix1 = new Matrix(3, 3);
			for (var i = 0; i < matrix1.GetLength(0); i++)
			{
				for (var j = 0; j < matrix1.GetLength(1); j++)
				{
					matrix1[i, j] = 1;
				}
			}
			
			var matrix2 = new Matrix(3, 3);
			for (var i = 0; i < matrix2.GetLength(0); i++)
			{
				for (var j = 0; j < matrix2.GetLength(1); j++)
				{
					matrix2[i, j] = i * j + i * 2;
				}
			}
			
			Console.WriteLine("matrix1: {0}", matrix1);
			Console.WriteLine("matrix2: {0}", matrix2);
			Console.WriteLine();
			
			// Adding and subtracting matrices
			Matrix sum = Matrix.Add(matrix1, matrix2);
			Matrix diff = Matrix.Subtract(matrix1, matrix2);
			Console.WriteLine("The sum of matrix1 and matrix2 equals: {0}", sum);
			Console.WriteLine("The difference between matrix1 and matrix2 equals: {0}", diff);
			
			sum = matrix1 + matrix2;
			diff = matrix1 - matrix2;
			Console.WriteLine("The sum of matrix1 and matrix2 equals: {0}", sum);
			Console.WriteLine("The difference between matrix1 and matrix2 equals: {0}", diff);
			
			Console.Write("The sum of matrix1 and matrix2 equals: ");
			matrix1.Add(matrix2);
			Console.WriteLine(matrix1);
			
			Console.Write("The difference between matrix1 and matrix2 equals: ");
			matrix1.Subtract(matrix2);
			Console.WriteLine(matrix1);
			Console.WriteLine();
			
			// Multiplying matrices
			Matrix product = Matrix.Multiply(matrix1, matrix2);
			Console.WriteLine("The product of matrix1 and matrix2 equals: {0}", product);
			
			product = matrix1 * matrix2;
			Console.WriteLine("The product of matrix1 and matrix2 equals: {0}", product);
			Console.WriteLine();
			
			// Multiply a matrix by a scalar
			product = 2 * matrix1;
			Console.WriteLine("matrix1 multiplied by 2 equals: {0}", product);
			
			Console.Write("matrix1 multiplied by 2 equals: ");
			matrix1.Multiply(2);
			Console.WriteLine(matrix1);
			Console.WriteLine();
			
			// Transpose of a matrix
			Matrix transposed = Matrix.Transpose(matrix2);
			Console.WriteLine("Transposed matrix2 equals {0}", transposed);
			Console.WriteLine();
			
			// Extracting a submatrix
			Matrix subMatrix = matrix2.SubMatrix(1, 1, 2, 2);
			Console.WriteLine("The submatrix extracted from matrix2 equals {0}", subMatrix);
			Console.WriteLine();
			
			// Comparing matrices
			bool equals = (matrix1 == matrix2);
			Console.WriteLine("{0} equals {1}: {2}", matrix1, matrix2, equals);
			
			matrix1 = new Matrix(3, 3);
			for (var i = 0; i < matrix1.GetLength(0); i++)
			{
				for (var j = 0; j < matrix1.GetLength(1); j++)
				{
					matrix1[i, j] = 1;
				}
			}
			
			matrix2 = new Matrix(3, 3);
			for (var i = 0; i < matrix2.GetLength(0); i++)
			{
				for (var j = 0; j < matrix2.GetLength(1); j++)
				{
					matrix2[i, j] = 1;
				}
			}
			
			equals = (matrix1 == matrix2);
			Console.WriteLine("{0} equals {1}: {2}", matrix1, matrix2, equals);
		}
	}
}